import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-doc-modal',
  templateUrl: './doc-modal.component.html',
  styleUrls: ['./doc-modal.component.scss'],
})
export class DocModalComponent implements OnInit {
  modalTitle: string;
  modelId: number;
  constructor(
    private modalController: ModalController,
    public navCtrl: NavController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
  }
  async closeModal($event) {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
  chat() {
    this.navCtrl.navigateForward("chat");
    this.closeModal("anything");
  }
}
