import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DocModalComponent } from '../doc-modal/doc-modal.component';

@Component({
  selector: 'app-doc-list',
  templateUrl: './doc-list.component.html',
  styleUrls: ['./doc-list.component.scss'],
})
export class DocListComponent implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {}
  
  async openModal() {
    const modal = await this.modalController.create({
      component: DocModalComponent,
      componentProps: {
        "paramID": 123,
        "paramTitle": "name"
      },
      cssClass: 'my-custom-modal-css'
    });

    // modal.onDidDismiss().then((dataReturned) => {
    //   if (dataReturned !== null) {
    //     this.dataReturned = dataReturned.data;
    //     //alert('Modal Sent Data :'+ dataReturned);
    //   }
    // });

    return await modal.present();
  }
}

