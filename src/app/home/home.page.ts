import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, ModalController } from '@ionic/angular';
import { UserService } from '../api/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data: any = '';
  constructor(public http: HttpClient,
    public plateform: Platform,
    public userService: UserService,
  ) {
    // this.plateform.ready().then(() =>{
    //   this.saveUserData();
    // })
  }
  saveUserData() {
    // var dataToSend = {
    //   first_name: 'akshay',
    //   last_name: 'singh',
    //   age: '24',
    //   gender: 'Male',
    //   email: 'akshay@gmail.com',
    //   phone: '995566332',
    //   password: 'Aickstun'
    // };
    // this.userService.saveUserData(dataToSend).subscribe(data => {
    //   this.data = JSON.stringify(data);
    //   console.log(this.data);
    // })
    // var  url = "http://localhost:3300/userData";
    // this.http.post(url,dataToSend,{ headers:new HttpHeaders({
    //   "content-Type":"application/json"
    // })});
  }
  getdata() {
    this.userService.getData().subscribe(data => {
      console.log(data);
    })
  }
}
