import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, } from '@ionic/angular';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  public doctor = false;
  constructor(private alertCtrl: AlertController,
    public navCtrl: NavController
    ) { }

  ngOnInit() {}

  myDoctor() {
    if (this.doctor === false) {
      this.doctor = true;
    } else {
      this.doctor = false;
    }
  }
  signUp() {
    this.navCtrl.navigateForward("sign-in");
  }
}


