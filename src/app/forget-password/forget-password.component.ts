import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgetPasswordComponent implements OnInit {
  public email: string = "";
  public rgx ='^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$';
  constructor(private alertCtrl: AlertController) { }

  ngOnInit() { }
  forgetPassword() {
    if (this.email === "") {
      this.presentAlert("enter the email");
    } else if (!this.email.match(this.rgx)) {
      this.presentAlert("enter the valid email");
    } else {
      this.presentAlert("We will send password to your Email");
    }
  }
  async presentAlert(message: string) {
    let alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: message,
      buttons: ['ok']
    });
    await alert.present();
  }
}
