import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DocListComponent } from '../doc-list/doc-list.component';
import { DocModalComponent } from '../doc-modal/doc-modal.component';

@Component({
  selector: 'app-main-user',
  templateUrl: './main-user.page.html',
  styleUrls: ['./main-user.page.scss'],
})
export class MainUserPage implements OnInit {
  dataReturned: any;
  constructor(  
    public modalController: ModalController,
    public navCtrl: NavController
    ) { }

  ngOnInit() {
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: DocModalComponent,
      componentProps: {
        "paramID": 123,
        "paramTitle": "name"
      },
      cssClass: 'my-custom-modal-css'
    });

    // modal.onDidDismiss().then((dataReturned) => {
    //   if (dataReturned !== null) {
    //     this.dataReturned = dataReturned.data;
    //     //alert('Modal Sent Data :'+ dataReturned);
    //   }
    // });

    return await modal.present();
  }
  category() {
    this.navCtrl.navigateForward("docList")
  }
}
