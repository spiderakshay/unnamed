import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) { }

  saveUserData(dataToSend) {
    var url = "http://localhost:3300/userData";
    return  this.http.post(url, dataToSend, {
      headers: new HttpHeaders({
        "content-Type": "application/json"
      })
    });
  }
  signIn(dataToSend) {
    var url = "http://localhost:3300/signIn";
    return  this.http.post(url,dataToSend, {
      headers: new HttpHeaders({
        "content-Type": "application/json"
      })
    });
  }
  getData() {
    var url = "http://localhost:3300/userData";
    return  this.http.get(url, {
      headers: new HttpHeaders({
        "content-Type": "application/json"
      })
    });
  }
}
