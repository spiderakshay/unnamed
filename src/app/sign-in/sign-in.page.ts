import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { UserService } from '../api/user.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {
  public doctor = false;
  public uName: string = "";
  public password: string = "";
  public data;
  public rgx: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$';
  constructor(
    private alertCtrl: AlertController,
    public userService: UserService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }
  check() {
    if (this.doctor === false) {
      this.doctor = true;
    } else {
      this.doctor = false;
    }
  }
  userSignIn() {
    if (this.uName === '') {
      this.presentAlert("Enter User Name");
    } else if (this.password === '') {
      this.presentAlert("Enter Password");
    } else if (!this.uName.match(this.rgx)) {
      this.presentAlert("Enter valid email");
    } else {
      var dataToSend = {
        username: this.uName,
        password: this.password
      };
      this.userService.signIn(dataToSend).subscribe(data => {
       // this.data = JSON.stringify(data);
        console.log(data[0].first_name);
      })
      this.presentAlert("Sucessfully SignIn");
      this.navCtrl.navigateForward("main-user");
    }
    this.uName = "";
    this.password = "";
  }
  doctorSignIn() {
    if (this.uName === '') {
      this.presentAlert("enter Email");
    } else if (this.password === '') {
      this.presentAlert("Enter Password");
    } else {

      this.presentAlert("sucessfully signIn");
    }
    this.uName = "";
    this.password = "";
  }
  async presentAlert(message: string) {
    let alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: message,
      buttons: ['ok']
    });
    await alert.present();
  }
}
